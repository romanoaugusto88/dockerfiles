DOCKER_BIN := `which docker`
REPOSITORY_OWNER := "romanoaugusto88"

base-builder:
	${DOCKER_BIN} build -t ${REPOSITORY_OWNER}/base-builder:latest base-builder

golang-builder: base-builder
	${DOCKER_BIN} build -t ${REPOSITORY_OWNER}/golang-builder:latest golang-builder
	${DOCKER_BIN} run --rm ${REPOSITORY_OWNER}/golang-builder:latest > golang-slim/rootfs.tar

golang-slim: golang-builder
	${DOCKER_BIN} build -t ${REPOSITORY_OWNER}/golang-slim:latest golang-slim

python-builder: base-builder
	${DOCKER_BIN} build -t ${REPOSITORY_OWNER}/python-builder:latest python-builder
	${DOCKER_BIN} run --rm ${REPOSITORY_OWNER}/python-builder:latest > python-slim/rootfs.tar

python-slim: python-builder
	${DOCKER_BIN} build -t ${REPOSITORY_OWNER}/python-slim:latest python-slim

.PHONY: base-builder golang-builder golang-slim python-builder python-slim
